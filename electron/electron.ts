import {app, BrowserWindow, ipcMain} from 'electron'
import * as path from 'path'

const main = () => {
  const mainWindow = new BrowserWindow({
    width: 500,
    height: 500,
    simpleFullscreen: true,
    title: 'Travian Bot Main Window',
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  ipcMain.on('setName', (e, v) => {
    const name = `${v}!!!`
    mainWindow.webContents.send('getName', name)
  })

  mainWindow.loadURL('http://localhost:5555')
}

app.on('ready', main)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})