import {ChangeEvent, FC, useState, useEffect} from 'react'

const {ipcRenderer} = window.require('electron')

export const App: FC = () => {
  const [name, setName] = useState('')
  const [data, setData] = useState<null | string>(null)

  useEffect(() => {
    ipcRenderer.on('getName', (e, v) => {
      setData(v)
    })
  }, [])

  const changeName = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.currentTarget.value)
  }

  const handlerSave = () => {
    ipcRenderer.send('setName', name)
  }

  return (
    <div style={{textAlign: 'center', fontSize: '48px'}}>
      Hello {data ? data : 'World'}

      <input type="text" placeholder={'your name'} value={name} onChange={changeName}/>
      <button onClick={handlerSave}>Save</button>
    </div>
  )
}